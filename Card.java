public class Card{
	private String suit;
	private int value;
	
	public Card(String suit, int value){
		this.suit = suit;
		this.value = value;
	}
	
	public String getSuit(){
		return this.suit;
	}
	
	public void setSuit(String suit){
		this.suit = suit;
	}
	
	public int getValue(){
		return this.value;
	}
	public void setValue(int value){
		this.value = value;
	}
	
	public String toString(){
		String valueToString = String.valueOf(value);
		
		if(value == 1){valueToString = "Ace";}
		if(value == 11){valueToString = "Jack";}
		if(value == 12){valueToString = "Queen";}
		if(value == 13){valueToString = "King";}
		
		return valueToString  + " of " + suit;
	}
}