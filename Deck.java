import java.util.Random;
public class Deck{
	
	private Card[] stack;
	private int numberOfCards;
	private Random rng;
	
	public Deck(){
		rng = new Random();
		int i = 0;
		this.numberOfCards = 52;
		this.stack = new Card[numberOfCards];
		//hearts U+2665
		//spades U+2660
		//clubs U+2663
		//diamonds U+2666
		
		String[] suits = new String[]{"hearts", "spades", "clubs", "diamonds"};
		int[] ranks = new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13};
		
		for(int s = 0; s < suits.length; s++){
			for(int r = 0; r < ranks.length; r++){
				stack[i] = new Card(suits[s], ranks[r]);
				i++;
			}
		}
	}
	
	public int length(){
		return this.numberOfCards;
	}
	
	/*public void setLength(int numberOfCards){
		this.numberOfCards = numberOfCards;
	}*/
	
	public Card drawTopCard(){
		this.numberOfCards = this.numberOfCards-1;
		return this.stack[this.numberOfCards];
	}
	
	public String toString(){
		String allCards = "";
		for(int i = 0; i < this.numberOfCards; i++){
			allCards += this.stack[i] + "\n";
		}
		return allCards;
	}
	
	public void shuffle(){
		for(int i = 0; i < this.numberOfCards; i++){
			int randomCard = rng.nextInt(this.numberOfCards);
			Card chosenCard = this.stack[randomCard];
			this.stack[randomCard] = this.stack[i];
			this.stack[i] = chosenCard;
		}
	}
}